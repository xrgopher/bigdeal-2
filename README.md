# 简介

bridge analyzer using deal generator and dds

# 安装

## Windows

* install python 3.6+ https://www.python.org/downloads/windows/ 
* download [redeal-master.zip](redeal-master.zip)

````
python -mpip install --user --upgrade redeal-master.zip
# change bid.py n=2000 => n=100
python bid.py
````

## Mac

* install python 3.6+ https://www.python.org/downloads/mac-osx/

````
python3 -mpip install --user --upgrade git+https://github.com/anntzer/redeal 
pip3 install git+https://github.com/anntzer/redeal target='path' # You have to designate a path else it will be some errors when import this package)//Keep Internet Connected! It takes some time to clone from webpage. It's better to upgrade pip and init git.
# change bid.py n=2000 => n=100
python3 bid.py
````

### 常见问题

* Mac: hang there during installation (github is ok to connect) // fixed

# 目标

## 第一周任务：环境和分析首攻lead.py

熟悉环境，理解现有的lead.py，结合致胜首攻一书，调试出和书上一样的结果

* @wingzero1984
* @owenji99

## 第二周任务：实战演练

结合致胜首攻一书，挑一到两个牌例，编出程序，调试出和书上一样的结果。学有余力的争取能独立完成网上发布

# 致谢

@坚心 @自由最重要 @tutulfy

# Reference

* [https://xrgopher.gitlab.io/bigdeal-2/](https://xrgopher.gitlab.io/bigdeal-2/)
* https://github.com/anntzer/redeal
* [A Simulation Tutorial for Better Decisionmaking at Bridge](http://datadaydreams.com/posts/a-simulation-tutorial-for-better-decisionmaking-at-bridge/)
* [【惊讶反转】大样本分析结果表明，3NT的成功率为64%，而5C/D的机会高达](https://mp.weixin.qq.com/s?__biz=MzAwNzA2NTU1NQ==&mid=2648781917&idx=1&sn=93ca8aba9e5cf0247b8565806a2709fd)